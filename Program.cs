﻿using System;

namespace ets
{
    class Program
    {
        static void Main(string[] args)
        {
            Converter converter = new Converter();
            double amount, result;
            string to;

            Console.Write("Amount: ");
            amount = double.Parse(Console.ReadLine());
            Console.Write("Convert to : ");
            to = Console.ReadLine();

            result = converter.GetConverter(to)(amount);
            
            Console.Write($"Result: {result} {to}");            
            
        }
    }
}
