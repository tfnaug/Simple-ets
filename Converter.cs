using System;

namespace ets
{
    public delegate double ConverterDelegate(double value);

    public class Converter
    {
        public double USD2IDR(double usd)
            => usd * 13780.70d;

        public double IDR2USD(double idr)
            => idr * 0.000073d;

        public double Default(double value)
        {
            Console.WriteLine("Invalid type of Currency");
            return value;
        }

    
        public ConverterDelegate GetConverter(string to)
        {
            switch(to)
            {
                case "USD":
                    return IDR2USD;
                case "IDR":
                    return USD2IDR;
                default:
                    return Default;

            }
        }



    
    }



}